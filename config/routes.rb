Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "/category/index" => "MealCategory#index"
  get "/category/create" => "MealCategory#create"
  get "/category/update/id" => "MealCategory#update"
  get "/category/show/id" => "MealCategory#show"
  get "/category/destroy/id" => "MealCategory#destroy"
  
  get "/meal/index" => "Meal#index"
  get "/meal/create" => "Meal#create"
  get "/meal/update/id" => "Meal#update"
  get "/meal/show/id" => "Meal#show"
  get "/meal/destroy/id" => "Meal#destroy"
  

end
