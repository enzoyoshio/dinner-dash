class MealsController < ApplicationController
    before_action :findar, only: [:show, :update, :destroy] 

    def index 
        @meal = Meal.all
        render json: @meal
    end

    def show
        render json: @meal
    end

    def create
        if @meal.save
            render json: @meal
        else
            render json: @meal.errors
        end 
    end

    def update
        if @meal.update
            render json: @meal
        else
            render json: @meal.errors
        end 
    end

    def destroy
        if @meal.delete
            render json: @meal
        else
            render json: @meal.errors
        end
    end

    private

    def findar
        @category = MealCategory.find(params[:id])
    end

    def category_params
        params.require(:Meal).permit([:name, :description, :price, :image, :available])
    end

end
