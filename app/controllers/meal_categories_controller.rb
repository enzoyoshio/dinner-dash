class MealCategoriesController < ApplicationController
    before_action :findar, only: [:show, :update, :destroy] 

    def index
        @category = MealCategory.all
        render json: @category
    end

    def show
        render json: @category
    end

    def update
        if @category.update
            render json: @category
        else
            render json: {msg:"erro"}
        end
    end

    def create
        @category = MealCategory.new(category_params)
        if @category.save
            render json: @category
        else
            render json: @category.errors
        end
    end

    def destroy
        if @category.delete
            render json: @category
        else
            render json: @category.errors
        end
    end

    private

    def findar
        @category = MealCategory.find(params[:id])
    end

    def category_params
        params.require(:MealCategory).permit([:name])
    end

end
