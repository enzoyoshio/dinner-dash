class Meal < ApplicationRecord
    has_one : MealCategory
    belongs_to : OrderMeal
end
